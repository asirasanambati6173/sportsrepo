INTRODUCTION
Sportsstore is Canada’s first professionally run integrated Sports & Fitness goods company. Sportsstore is amalgamation of an experienced professional team, Canada’s best known sporting icons & industry experts to drive sporting revolution in India. Incorporated in 2020, Sportsstore operates on a very unique hybrid business model with the mission to become the first port of call for sports & fitness needs of consumers in Canada.

TECHNOLOGIES USED
Html
Css
Javascript

INSTALLATIONS
Visual Studio Code

USAGE
It's a complete ecommerce website with all professional sports stuff that will be available which has user friendly GUI so that it is easy to understand to buy the sports materials in the website.


LICENSE
Copyright (c) 2020 @Anudeep Sirasanambati. Everyone is permitted to copy and distribute verbatim copies of this license document, but changing the same is not allowed.

